import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../actions/itemActions'
import Items from '../components/ItemComponent'
export class ItemContainer extends Component {
    componentDidMount() {
        this.props.initLoad()
    }
  render() {
    return (
      <Items {...this.props}/>
    )
  }
}

const mapStateToProps = (store) => {
    return{
        items: store.items.listItems,
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        initLoad:() => {
            dispatch(actions.getItemRequest())
        },
        addItem: (data) => {
            dispatch(actions.addItemRequest(data))
        },
        deleteItem: (data) => {
            dispatch(actions.deleteItemRequest(data))
        },
        updateItem: (data) => {
            dispatch(actions.updateItemRequest(data))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemContainer)