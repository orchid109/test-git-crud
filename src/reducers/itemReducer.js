import * as types from '../constants'

const DEFAULT_STATE = {
    listItem: [],
    isLoading: false,
    error: false,
    errorMessage: null
}

const reducer = (state = DEFAULT_STATE, action) => {
    switch (action.type) { 
        case types.GET_ITEM_REQUEST:
        case types.ADD_ITEM_REQUEST:
        case types.DELETE_ITEM_REQUEST:
        case types.UPDATE_ITEM_REQUEST:
             state.isLoading = true
            
        case types.GET_ITEM_SUCCESS:
            state.isLoading = false,
            state.listItem = action.payload.listItem
        case types.ADD_ITEM_SUCCESS:
        case types.DELETE_ITEM_SUCCESS:
        case types.UPDATE_ITEM_SUCCESS:
            state.isLoading = false
        
        case types.GET_ITEM_FAILURE:
        case types.ADD_ITEM_FAILURE:
        case types.DELETE_ITEM_FAILURE:
        case types.UPDATE_ITEM_FAILURE:
            state.error = true,
            state.errorMessage = action.payload.errorMessage
    }
}

export default reducer